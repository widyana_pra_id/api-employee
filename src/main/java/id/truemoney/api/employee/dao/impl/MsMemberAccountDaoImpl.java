package id.truemoney.api.employee.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import id.truemoney.api.employee.dao.MsMemberAccountDao;
import id.truemoney.api.employee.datasource.DataSourceConnectionPostgres;
import id.truemoney.api.employee.model.MsMemberAccountModel;

public class MsMemberAccountDaoImpl implements MsMemberAccountDao {
	Logger log = Logger.getLogger(MsMemberAccountDaoImpl.class);

	public MsMemberAccountModel inquiryMemberData(String idMember) {
		PreparedStatement ps = null;
		Connection conn = null;
		ResultSet rs = null;
		MsMemberAccountModel memberAccountModel = new MsMemberAccountModel();
		try {
			conn = DataSourceConnectionPostgres.getInstance().getDataSource().getConnection();
			String query = "SELECT a.id_member, a.\"Nama\", a.\"Handphone\", a.\"NomorKartu\" , b.\"NomorKTP\", b.\"Email\" FROM \"MsMemberAccount\" as a JOIN \"MsMember\" as b ON a.id_member = b.id_member where a.id_member = ?";
			ps = conn.prepareStatement(query);
			ps.setString(1, idMember);
			rs = ps.executeQuery();
			if (rs.next()) {
				memberAccountModel.setIdMember(rs.getString("id_member"));
				memberAccountModel.setNama(rs.getString("Nama"));
				memberAccountModel.setNohp(rs.getString("Handphone"));
				memberAccountModel.setNomorKartu(rs.getString("NomorKartu"));
				memberAccountModel.setNoKtp(rs.getString("NomorKTP"));
				memberAccountModel.setEmail(rs.getString("Email"));
				memberAccountModel.setResponseCode("INQ-000");
			}
		} catch (Exception e) {
			log.error("Error inquiry ", e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e2) {
				log.error("error close connection inquiry", e2);
			}
		}
		return memberAccountModel;
	}

	public int updateMemberAccount(String idMember, String NomorKartu, Connection conn) {
		int hasil = 0;
		PreparedStatement ps = null;
//		Connection conn = null;
		try {
			String query = "UPDATE \"MsMemberAccount\" SET \"NomorKartu\" = ?, id_tipeaccount = 3, id_accountstatus = 1, id_statusverifikasi = 1   WHERE id_member = ?";
			System.out.println(query);
			ps = conn.prepareStatement(query);
			ps.setString(1, NomorKartu);
			ps.setString(2, idMember);
			hasil = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("update error ",e);
		} 
		finally {
			try {
				if (ps != null) {
					ps.close();
				}
//				if (conn != null) {
//					conn.close();
//				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return hasil;
	}

}
