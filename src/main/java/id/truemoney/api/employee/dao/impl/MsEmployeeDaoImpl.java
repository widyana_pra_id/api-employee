package id.truemoney.api.employee.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import id.truemoney.api.employee.dao.MsEmployeeDao;
import id.truemoney.api.employee.datasource.DataSourceConnectionPostgres;
import id.truemoney.api.employee.model.EmployeeModel;

public class MsEmployeeDaoImpl implements MsEmployeeDao {
	Logger log = Logger.getLogger(MsEmployeeDaoImpl.class);

	public int saveEmployee(EmployeeModel employeeModel, Connection conn) {
		int hasil = 0;
		PreparedStatement ps = null;
//		Connection conn = null;
		try {
//			conn = DataSourceConnectionPostgres.getInstance().getDataSource().getConnection();
			String query = "INSERT INTO \"public\".\"MsEmployee\" (\r\n" + "	\"ID_Employee\",\r\n"
					+ "	\"Nama\",\r\n" + "	\"FlagActive\",\r\n" + "	\"TimeStamp\"\r\n" + ")\r\n" + "VALUES\r\n"
					+ "	(\r\n" + "		?,\r\n" + "		?,\r\n" + "		't',\r\n" + "		now()\r\n" + "	);\r\n"
					+ "";
			System.out.println(query);
			System.out.println(employeeModel.getIdEmployee());
			System.out.println(employeeModel.getNama());
			ps = conn.prepareStatement(query);
			ps.setString(1, employeeModel.getIdEmployee());
			ps.setString(2, employeeModel.getNama());
			hasil = ps.executeUpdate();
		} catch (Exception e) {
			log.error("Error Employee", e);
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				} 
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return hasil;
	}

	public boolean isExist(String employeeID, Connection conn) {
		// TODO Auto-generated method stub
		boolean isExist = false;
		PreparedStatement ps = null;
//		Connection conn = null;
		ResultSet rs = null;
		try {
//			conn = DataSourceConnectionPostgres.getInstance().getDataSource().getConnection();
			String query = "SELECT \"ID_Employee\" FROM \"MsEmployee\" WHERE \"ID_Employee\" = ?";
			ps = conn.prepareStatement(query);
			ps.setString(1, employeeID);
			rs = ps.executeQuery();
			if (rs.next()) {
				isExist = true;
			}
			
		} catch (Exception e) {
			log.error("Error check employee ",e);
		} finally {
			try {
				if (ps != null) {
					ps.close();
				} 
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		
		return isExist;
	}

}
