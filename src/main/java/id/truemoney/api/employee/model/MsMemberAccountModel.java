package id.truemoney.api.employee.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MsMemberAccountModel {
	@Expose
	@SerializedName("nama")
	private String nama;

	@Expose
	@SerializedName("nohp")
	private String nohp;

	@Expose
	@SerializedName("email")
	private String email;

	@Expose
	@SerializedName("no_ktp")
	private String noKtp;

	@Expose
	@SerializedName("nomor_kartu")
	private String nomorKartu;

	@Expose
	@SerializedName("rc")
	private String responseCode;

	@Expose
	@SerializedName("id_member")
	private String idMember;

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNohp() {
		return nohp;
	}

	public void setNohp(String nohp) {
		this.nohp = nohp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNoKtp() {
		return noKtp;
	}

	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}

	public String getNomorKartu() {
		return nomorKartu;
	}

	public void setNomorKartu(String nomorKartu) {
		this.nomorKartu = nomorKartu;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getIdMember() {
		return idMember;
	}

	public void setIdMember(String idMember) {
		this.idMember = idMember;
	}

}
