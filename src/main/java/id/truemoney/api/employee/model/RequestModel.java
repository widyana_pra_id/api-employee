package id.truemoney.api.employee.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestModel {
	@Expose
	@SerializedName("id_member")
	private String idMember;

	@Expose
	@SerializedName("no_kartu")
	private String noKartu;

	public String getIdMember() {
		return idMember;
	}

	public void setIdMember(String idMember) {
		this.idMember = idMember;
	}

	public String getNoKartu() {
		return noKartu;
	}

	public void setNoKartu(String noKartu) {
		this.noKartu = noKartu;
	}

}
